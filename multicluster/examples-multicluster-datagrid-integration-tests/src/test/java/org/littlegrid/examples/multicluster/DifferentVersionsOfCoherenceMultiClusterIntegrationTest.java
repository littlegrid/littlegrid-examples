package org.littlegrid.examples.multicluster;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.InvocationService;
import com.tangosol.net.NamedCache;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.littlegrid.ClusterMemberGroup;
import org.littlegrid.ClusterMemberGroupUtils;
import org.littlegrid.support.ExtendUtils;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * Integration test demonstrating different versions of Coherence running in different clusters
 * using littlegrid.
 * <p/>
 * If you want to run this test class in your IDE, then you'll probably need to increase the
 * memory and perm-space size.
 */
public class DifferentVersionsOfCoherenceMultiClusterIntegrationTest {
    private static final int PORT_OFFSET_FOR_OLDER = 3000;

    private static final int NUMBER_OF_JMX_MEMBERS = 1;

    private static final int NUMBER_OF_STORAGE_ENABLED_FOR_OLDER = 1;
    private static final int NUMBER_OF_EXTEND_PROXY_FOR_OLDER = 1;

    private static final int EXPECTED_NUMBER_FOR_OLDER = NUMBER_OF_STORAGE_ENABLED_FOR_OLDER
            + NUMBER_OF_EXTEND_PROXY_FOR_OLDER + NUMBER_OF_JMX_MEMBERS;

    private static final int NUMBER_OF_STORAGE_ENABLED_FOR_NEWER = 2;

    private static final int NUMBER_OF_EXTEND_PROXY_FOR_NEWER = 2;

    private static final int EXPECTED_NUMBER_FOR_NEWER = NUMBER_OF_STORAGE_ENABLED_FOR_NEWER
            + NUMBER_OF_EXTEND_PROXY_FOR_NEWER + NUMBER_OF_JMX_MEMBERS;

    private static final int FAST_START_JOIN_TIMEOUT_MILLISECONDS_FOR_OLDER = 1000;
    private static final int FAST_START_JOIN_TIMEOUT_MILLISECONDS_FOR_NEWER = 100;

    private static final int LOG_LEVEL = 6;
    private static final Logger LOGGER =
            Logger.getLogger(DifferentVersionsOfCoherenceMultiClusterIntegrationTest.class.getName());

    private static ClusterMemberGroup newerMemberGroup = null;

    private static ClusterMemberGroup olderMemberGroup = null;

    private final InvocationService invocationServiceForOlder =
            (InvocationService) CacheFactory.getService("older-invocation-service");

    private final InvocationService invocationServiceForNewer =
            (InvocationService) CacheFactory.getService("newer-invocation-service");
    private final NamedCache cacheInOlder = CacheFactory.getCache("cache-in-older");
    private final NamedCache cacheInNewer = CacheFactory.getCache("cache-in-newer");


    @BeforeClass
    public static void beforeTests() {
        // Note: the setup for this set of tests is a bit more involved than usual as the intention is to
        // demonstrate a number of features and concepts

        // Start the cluster with the newer version of Coherence
        //
        // Note: this cluster has more than one Extend proxy so that we can stop one later on to
        // demonstrate Coherence's fail-over ability
        final ClusterMemberGroup.Builder builder = ClusterMemberGroupUtils.newBuilder()
                .setStorageEnabledCount(NUMBER_OF_STORAGE_ENABLED_FOR_NEWER)
                .setExtendProxyCount(NUMBER_OF_EXTEND_PROXY_FOR_NEWER)
                .setJmxMonitorCount(NUMBER_OF_JMX_MEMBERS)
                .setLogLevel(LOG_LEVEL)
                .setFastStartJoinTimeoutMilliseconds(FAST_START_JOIN_TIMEOUT_MILLISECONDS_FOR_NEWER)
                        // Excluding the older version of Coherence - so only the new version is on class-path
                .setJarsToExcludeFromClassPath("coherence-3.7.0.0");
        newerMemberGroup = builder.buildAndConfigureForNoClient();


        // Work out port offsets, then the second cluster won't clash ports with the new version already running
        final int wkaPortForOlder = builder.getWkaPort() + PORT_OFFSET_FOR_OLDER;
        final int extendPortForOlder = builder.getExtendPort() + PORT_OFFSET_FOR_OLDER;

        LOGGER.warning(format("WKA port of: '%s' and Extend port of '%s' will be used for second cluster",
                wkaPortForOlder, extendPortForOlder));


        // Start the cluster with the older version of Coherence, this time we build for an Extend client
        olderMemberGroup = ClusterMemberGroupUtils.newBuilder()
                .setStorageEnabledCount(NUMBER_OF_STORAGE_ENABLED_FOR_OLDER)
                .setWkaPort(wkaPortForOlder)
                .setExtendProxyCount(NUMBER_OF_EXTEND_PROXY_FOR_OLDER)
                .setExtendPort(extendPortForOlder)
                .setJmxMonitorCount(1)
                .setLogLevel(LOG_LEVEL)
                .setFastStartJoinTimeoutMilliseconds(FAST_START_JOIN_TIMEOUT_MILLISECONDS_FOR_OLDER)
                        // Excluding the newer version of Coherence - so only old version is on class-path
                .setJarsToExcludeFromClassPath("coherence-3.7.1.5")
                .setClientCacheConfiguration("multicluster-extend-cache-config.xml")
                        // The cluster running the older version of Coherence only has a single Extend proxy,
                        // whilst the cluster running the newer version of Coherence has several Extend proxy
                        // servers, therefore we want to configure the Extend client so that it knows about
                        // more than one Extend proxy in the newer cluster
                .setAdditionalSystemProperty("older.tangosol.coherence.extend.port", extendPortForOlder)
                .setAdditionalSystemProperty("newer.tangosol.coherence.extend.port.1", builder.getExtendPort())
                .setAdditionalSystemProperty("newer.tangosol.coherence.extend.port.2", builder.getExtendPort() + 1)
                .buildAndConfigureForExtendClient();

    }

    @AfterClass
    public static void afterTests() {
        ClusterMemberGroupUtils.shutdownCacheFactoryThenClusterMemberGroups(newerMemberGroup, olderMemberGroup);
    }

    @Test
    public void extendClientConnectingToDifferentVersionsOfCoherence() {
        final String olderVersion =
                ExtendUtils.getExtendProxyMemberVersionThatClientIsConnectedTo(invocationServiceForOlder);

        final String newerVersion =
                ExtendUtils.getExtendProxyMemberVersionThatClientIsConnectedTo(invocationServiceForNewer);

        System.out.println("~*~* Older version: " + olderVersion + ", newer version: " + newerVersion);

        assertThat(newerVersion, not(olderVersion));
    }

    @Test
    public void extendClientCheckingTheDifferentClusterSizes() {
        final int olderClusterSize =
                ExtendUtils.getClusterSizeThatExtendClientIsConnectedTo(invocationServiceForOlder);

        System.out.println("~*~* Size of cluster running older version of Coherence is: " + olderClusterSize);

        assertThat(olderClusterSize, is(EXPECTED_NUMBER_FOR_OLDER));


        final int newerClusterSize =
                ExtendUtils.getClusterSizeThatExtendClientIsConnectedTo(invocationServiceForNewer);

        System.out.println("~*~* Size of cluster running newer version of Coherence is: " + newerClusterSize);

        assertThat(newerClusterSize, is(EXPECTED_NUMBER_FOR_NEWER));
        assertThat(newerClusterSize, not(olderClusterSize));
    }

    @Test
    public void putAndSize() {
        final int numberOfEntries = 10;

        assertThat(cacheInOlder.size(), is(0));
        assertThat(cacheInNewer.size(), is(0));

        for (int i = 0; i < numberOfEntries; i++) {
            cacheInOlder.put(i, i);
        }

        assertThat(cacheInOlder.size(), is(numberOfEntries));
        assertThat(cacheInNewer.size(), is(0));
    }

    @Test
    public void extendClientFailOverAfterProxyItIsConnectedToIsStopped()
            throws InterruptedException {

        final int memberIdConnectedToBeforeStop =
                ExtendUtils.getExtendProxyMemberIdThatClientIsConnectedTo(invocationServiceForNewer);

        System.out.println("~*~* Connected to member: " + memberIdConnectedToBeforeStop + ", will now stop it!");

        newerMemberGroup.stopMember(memberIdConnectedToBeforeStop);

        final int memberIdConnectedToAfterStop =
                ExtendUtils.getExtendProxyMemberIdThatClientIsConnectedTo(invocationServiceForNewer);

        System.out.println("~*~* Now connected to member: " + memberIdConnectedToAfterStop);

        final int sleepDuration = newerMemberGroup.getSuggestedSleepAfterStopDuration();

        System.out.println("~*~* About to now sleep for " + sleepDuration + " seconds to allow the "
                + "TCMP cluster members to figure out that a member has left - it takes a second or so");

        // Give Coherence a chance to work out that a member has left the TCMP cluster
        TimeUnit.SECONDS.sleep(sleepDuration);

        final int newerClusterSizeAfterStop =
                ExtendUtils.getClusterSizeThatExtendClientIsConnectedTo(invocationServiceForNewer);

        System.out.println("~*~* Size of cluster running newer version of Coherence is: " + newerClusterSizeAfterStop);

        assertThat(newerClusterSizeAfterStop, is(EXPECTED_NUMBER_FOR_NEWER - 1));
    }
}
